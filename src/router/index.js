import VueRouter from 'vue-router'
import Calculator from "../components/Calculator";

const routes = [
    {
        path: '/',
        name: "calculator",
        component: Calculator
    }
];

const router = new VueRouter({
    routes,
    mode: 'history'
})

export default router