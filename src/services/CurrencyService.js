import axios from 'axios'

class CurrencyService {
    _URL = 'http://api.nbp.pl/api/exchangerates/tables/C/'
    EXCHANGE_URL = 'http://api.nbp.pl/api/exchangerates/rates/c/'

    async getData() {
        // eslint-disable-next-line no-useless-catch
        try {
            return await axios.get(this._URL)
        } catch (e) {
            throw e
        }
    }

    async getTodayData() {
        // eslint-disable-next-line no-useless-catch
        try {
            return await this.axios.get(`${this._URL}`)
        } catch (e) {
            throw e
        }
    }

    async getLastDayOfMonth() {
        // eslint-disable-next-line no-useless-catch
        try {
            return null;
            // eslint-disable-next-line no-unreachable
        } catch (e) {
            throw e

        }
    }

    async getExchangeRate(code) {
        // eslint-disable-next-line no-useless-catch
        try {
            return await axios.get(`${this.EXCHANGE_URL}${code}/`)
        } catch (e) {
            throw e
        }
    }
}

export default CurrencyService;